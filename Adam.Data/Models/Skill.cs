﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adam.Data.Models
{
    public class Skill
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
    }
}
