﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adam.Data.Models
{
    public class Project
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Link { get; set; }
    }
}
