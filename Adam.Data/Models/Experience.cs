using System;
using System.Collections.Generic;

namespace Adam.Data.Models
{
    public class Experience
    {
        public int Id { get; set; }
        public string Workplace { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Role { get; set; }
        public string Description { get; set; }
    }
}